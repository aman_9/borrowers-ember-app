import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.store.createRecord('article', {
      friend: this.modelFor('friends/show')
    });
  },
  actions: {
    save() {
      this.modelFor('articles/new')
        .save().then(() => {
          this.transitionTo('articles');
        }
      );
    },
    cancel() {
      this.transitionTo('articles');
    }
  }
});
